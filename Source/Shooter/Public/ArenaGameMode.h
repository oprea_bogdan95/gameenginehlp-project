#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ArenaGameMode.generated.h"

UCLASS()
class SHOOTER_API AArenaGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	void IncreseEnemyCounter();
	void DecreseEnemyCounter();
	bool CanEnemiesStillSpwan();

protected:
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "GameSettings")
	int MaxSpawnCount = 10;

private:

	int EnemyCounter = 0;
};
