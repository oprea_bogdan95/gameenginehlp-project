#include "ArenaCharacterStats.h"

#include "Engine.h"

#include "ReflectionUtilsFunctionLibrary.h"

#pragma optimize("", off)

UArenaCharacterStats::UArenaCharacterStats()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UArenaCharacterStats::UpdateStat(const FString& StatName, float StatValue)
{
	// Example
	//		StatName:	"HP"
	//		InPath:		"This." + StatName

	FString Path = "This";
	Path += ".";
	Path += StatName;

	void* OutObject = nullptr;
	FProperty* StatProperty = UReflectionUtilsFunctionLibrary::RetrieveProperty(this, Path, OutObject);

	if (StatProperty != nullptr)
	{
		FFloatProperty* FloatProperty = CastField<FFloatProperty>(StatProperty);
		if (FloatProperty != nullptr)
		{
			float Value = FMath::Clamp(StatValue, 0.f, 1.f);
			FloatProperty->SetPropertyValue_InContainer(OutObject, Value);

			if (GEngine != nullptr)
			{
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("Stats updated: ") + FloatProperty->GetName());
			}
		}
	}
}

#pragma optimize("", on)