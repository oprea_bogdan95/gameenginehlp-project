#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "ArenaAIController.generated.h"

class AArenaCharacter;
class AArenaEnemyCharacter;

UCLASS()
class SHOOTER_API AArenaAIController : public AAIController
{
	GENERATED_BODY()

public:
	
	AArenaAIController();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void MoveTwordsPlayer();

	void LookAtPlayer(float DeltaTime);
	bool IsPlayerInRange();

protected:

	virtual void OnPossess(APawn* AIPawn) override;

private:
	
	UPROPERTY()
	AArenaCharacter* Player;

	FTimerHandle PlayerSerchTimerHandel;
	FTimerDelegate TimerDel;

private:

	UPROPERTY()
	AArenaEnemyCharacter* EnemyPawn;

	bool IsChasingPlayer = false;
};
