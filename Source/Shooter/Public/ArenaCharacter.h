#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ArenaCharacter.generated.h"

class UArenaCharacterStats;
class UArenaCharacterInventory;
class AArenaBaseWeapon;

UCLASS()
class SHOOTER_API AArenaCharacter : public ACharacter
{
	GENERATED_BODY()

public:

	AArenaCharacter();

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void BeginPlay() override;
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

private:

	void MoveUp(float Value);
	void MoveRight(float Value);
	void LookUp(float Value);
	void LookRight(float Value);
	void Fire();
	void StopFire();
	void SpawnWeapon();
private:

	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UArenaCharacterStats* StatsComponent;
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UArenaCharacterInventory* InventoryComponent;
	UPROPERTY(EditAnywhere, Category = "Weapon")
	TSubclassOf<AActor> Weapon;
	UPROPERTY(VisibleAnywhere)
	AArenaBaseWeapon* Gun;

	float RotationSpeed;
};
