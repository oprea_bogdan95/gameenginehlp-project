#include "ArenaCharacterInventory.h"

#include "ArenaCharacterStats.h"
#include "ArenaItem.h"
#include "ArenaItemDataAsset.h"
#include "ArenaItemStatModifier.h"

#include "ArenaLogContext.h"

#pragma optimize("", off)

UArenaCharacterInventory::UArenaCharacterInventory()
{
	PrimaryComponentTick.bCanEverTick = true;

	bWantsInitializeComponent = true;
}

void UArenaCharacterInventory::InitializeComponent()
{
	Super::InitializeComponent();

	AActor* Owner = GetOwner();
	if (Owner != nullptr)
	{
		CharacterStats = Owner->FindComponentByClass<UArenaCharacterStats>();
	}
}

void UArenaCharacterInventory::UninitializeComponent()
{
	Super::UninitializeComponent();

	CharacterStats = nullptr;
}

void UArenaCharacterInventory::AddItem(AArenaItem* i_Item)
{
	if (CharacterStats == nullptr)
	{
		UE_LOG(LogArena, Warning, TEXT("Missing Character Stats"));
		return;
	}

	if (i_Item == nullptr)
	{
		UE_LOG(LogArena, Warning, TEXT("Invalid Item"));
		return;
	}

	UArenaItemDataAsset* DataAsset = i_Item->ItemData;

	if (DataAsset == nullptr)
	{
		UE_LOG(LogArena, Warning, TEXT("Invalid Itam DataAsset"));
		return;
	}

	UE_LOG(LogArena, Log, TEXT("Added Item %s"), *DataAsset->ItemName.ToString());

	UDataTable* Modifiers = DataAsset->StatModifiers;

	if (Modifiers == nullptr)
	{
		UE_LOG(LogArena, Warning, TEXT("Invalid Item DataTable"));
		return;
	}

	static const FString ContextString(TEXT("UArenaCharacterInventory::AddItem"));

	TArray<FArenaItemStatModifier*> Rows;
	Modifiers->GetAllRows(ContextString, Rows);

	int NumRows = Rows.Num();

	for (size_t RowIndex = 0; RowIndex < NumRows; ++RowIndex)
	{
		FArenaItemStatModifier* Row = Rows[RowIndex];
		CharacterStats->UpdateStat(Row->StatName, Row->Value);
	}
}

#pragma optimize("", on)