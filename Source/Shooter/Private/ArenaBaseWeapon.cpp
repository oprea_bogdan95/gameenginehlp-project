#include "ArenaBaseWeapon.h"
#include "ArenaProjectile.h"
#include "Engine/Engine.h"
#include "ArenaCharacter.h"

AArenaBaseWeapon::AArenaBaseWeapon()
{
	PrimaryActorTick.bCanEverTick = true;

	MeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Root Mesh"));
	RootComponent = MeshWeapon;
	WeaponDirection = CreateDefaultSubobject<UArrowComponent>(TEXT("Weapon Direction"));
	WeaponDirection->SetupAttachment(MeshWeapon);
	WeaponDirection->ArrowSize = 0.5f;

	const FVector GunDirCompLocation = FVector(0.f, 50.f, 10.f);
	const FRotator GunDirCompRotation = FVector(0.f, 0.f, 90.f).Rotation();
	WeaponDirection->SetRelativeLocationAndRotation(GunDirCompLocation, GunDirCompRotation);
	WeaponFireRate = 0.5f;
}
void AArenaBaseWeapon::StartFire()
{
	if (!GetWorld()->GetTimerManager().IsTimerActive(ShootTimerHandle)) {
		GetWorld()->GetTimerManager().SetTimer(ShootTimerHandle, this, &AArenaBaseWeapon::Fire, WeaponFireRate, true);
	}
}

void AArenaBaseWeapon::StopFire()
{
	if (GetWorld()->GetTimerManager().IsTimerActive(ShootTimerHandle)) {
		GetWorld()->GetTimerManager().ClearTimer(ShootTimerHandle);
	}
}

void AArenaBaseWeapon::Fire()
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = GetOwner();
	SpawnParams.Instigator = GetInstigator();

	const FTransform transform = WeaponDirection->GetComponentTransform();

	AArenaProjectile* Bullet = GetWorld()->SpawnActor<AArenaProjectile>(Projectile, transform.GetLocation(), transform.Rotator(), SpawnParams);

	if (Bullet)
	{
		const FVector FireDirection = transform.Rotator().Vector();
		Bullet->ProjectileDirection(FireDirection);
	}
}
void AArenaBaseWeapon::SetPlayer(AArenaCharacter& _Player)
{
	this->Player = &_Player;
}

void AArenaBaseWeapon::BeginPlay()
{
	Super::BeginPlay();
}

void AArenaBaseWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

