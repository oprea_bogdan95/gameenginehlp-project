#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ArenaEnemySpawner.generated.h"

class AArenaEnemyCharacter;
class AArenaGameMode;
UCLASS()
class SHOOTER_API AArenaEnemySpawner : public AActor
{
	GENERATED_BODY()
	
public:	

	AArenaEnemySpawner();

protected:
	
	virtual void BeginPlay() override;

public:	
	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void Spawn();

protected:

	UPROPERTY(EditAnywhere, Category = "Spawn")
	float SpawnRate = 5.f;
	UPROPERTY(EditAnywhere, Category = "Spawn")
	int MaxSpawnCount = 5;
	UPROPERTY(EditAnywhere, Category = "Spawn")
	TSubclassOf<AArenaEnemyCharacter> PawnToSpawn;

private:

	FTimerHandle SpawnTimerHandle;

	UPROPERTY()
	AArenaGameMode* GameMode;
};
