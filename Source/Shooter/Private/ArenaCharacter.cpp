#include "ArenaCharacter.h"

#include "ArenaCharacterStats.h"
#include "ArenaCharacterInventory.h"
#include "ArenaItem.h"
#include "ArenaBaseWeapon.h"

AArenaCharacter::AArenaCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	bUseControllerRotationYaw = false;

	StatsComponent = CreateDefaultSubobject<UArenaCharacterStats>(TEXT("Stats"));
	InventoryComponent = CreateDefaultSubobject<UArenaCharacterInventory>(TEXT("Inventory"));

	RotationSpeed = 2.0f;
}

void AArenaCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	check(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveUp"), this, &AArenaCharacter::MoveUp);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AArenaCharacter::MoveRight);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &AArenaCharacter::LookUp);
	PlayerInputComponent->BindAxis(TEXT("LookRight"), this, &AArenaCharacter::LookRight);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &AArenaCharacter::Fire);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Released, this, &AArenaCharacter::StopFire);
}
void AArenaCharacter::BeginPlay()
{	
	Super::BeginPlay();
	SpawnWeapon();
}
void AArenaCharacter::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	if (OtherActor == nullptr)
	{
		return;
	}

	bool bIsAnItem = OtherActor->IsA<AArenaItem>();

	if (bIsAnItem)
	{
		AArenaItem* Item = Cast<AArenaItem>(OtherActor);
		InventoryComponent->AddItem(Item);

		Item->Destroy();
	}
}

void AArenaCharacter::MoveUp(float Value)
{
	if (Controller == nullptr || Value == 0.0) return;

	AddMovementInput(FVector::ForwardVector, Value);
}

void AArenaCharacter::MoveRight(float Value)
{
	if (Controller == nullptr || Value == 0.0) return;

	AddMovementInput(FVector::RightVector, Value);
}

void AArenaCharacter::LookUp(float Value)
{
	if (Controller == nullptr || Value == 0.0) return;

	AddControllerYawInput(Value * RotationSpeed);
}

void AArenaCharacter::LookRight(float Value)
{
	if (Controller == nullptr || Value == 0.0) return;

	AddControllerPitchInput(Value * RotationSpeed);
}

void AArenaCharacter::Fire()
{
	Gun->StartFire();
}

void AArenaCharacter::StopFire()
{
	Gun->StopFire();
}

void AArenaCharacter::SpawnWeapon()
{
	const FVector Location(0.0f, 0.0f, 0.0f);
	const FRotator Rotation(0.0f, 0.0f, 0.0f);
	Gun = Cast<AArenaBaseWeapon>(GetWorld()->SpawnActor<AActor>(Weapon, Location, Rotation));

	Gun->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, "GunSocket");
	
	Gun->SetPlayer(*this);
}

