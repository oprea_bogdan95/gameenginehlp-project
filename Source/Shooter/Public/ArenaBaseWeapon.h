#pragma once

#include "CoreMinimal.h"
#include "Components/ArrowComponent.h"
#include "GameFramework/Actor.h"
#include "ArenaBaseWeapon.generated.h"

class AArenaProjectile;
class AArenaCharacter;

UCLASS()
class SHOOTER_API AArenaBaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:	

	AArenaBaseWeapon();
	void StartFire();
	void StopFire();
	void Fire();
	void SetPlayer(AArenaCharacter& _Player);

protected:
	
	virtual void BeginPlay() override;

public:	
	
	virtual void Tick(float DeltaTime) override;

protected:

	UPROPERTY(EditDefaultsOnly, Category = "Projectile")
	TSubclassOf<AArenaProjectile> Projectile;
	UPROPERTY(VisibleAnywhere, Category = "Component")
	USkeletalMeshComponent* MeshWeapon;
	UPROPERTY(VisibleAnywhere, Category = "Component")
	UArrowComponent* WeaponDirection;
	UPROPERTY(VisibleAnywhere)
	AArenaCharacter* Player;

	float WeaponFireRate;
	FTimerHandle ShootTimerHandle;
};
