#include "ArenaAIController.h"
#include "Kismet/KismetSystemLibrary.h"
#include "ArenaCharacter.h"
#include "ArenaEnemyCharacter.h"

AArenaAIController::AArenaAIController() {
	PrimaryActorTick.bCanEverTick = true;
}

void AArenaAIController::BeginPlay()
{
	Super::BeginPlay();
	Player = Cast<AArenaCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	if (Player) {
		TimerDel.BindUFunction(this, FName("MoveTwordsPlayer"));
		GetWorld()->GetTimerManager().SetTimer(PlayerSerchTimerHandel, TimerDel, 1.0f, true);
	}
}

void AArenaAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsChasingPlayer) {
		
		LookAtPlayer(DeltaTime);

		if (!EnemyPawn)return;
		
		if (IsPlayerInRange()) {
			EnemyPawn->PlayerInRange();
		}
		else {
			EnemyPawn->PlayerOutOfRange();
		}
	}
}

void AArenaAIController::LookAtPlayer(float DeltaTime)
{
	SetFocalPoint(Player->GetTransform().GetLocation());
	UpdateControlRotation(DeltaTime);
}

void AArenaAIController::MoveTwordsPlayer()
{
	MoveToActor(Player, 250.0f);
	IsChasingPlayer = true;
}

bool AArenaAIController::IsPlayerInRange()
{
	EPathFollowingStatus::Type pl = GetMoveStatus();
	return pl == EPathFollowingStatus::Type::Idle;
}

void AArenaAIController::OnPossess(APawn* AIPawn)
{
	Super::OnPossess(AIPawn);
	EnemyPawn = Cast<AArenaEnemyCharacter>(AIPawn);
}
