#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "ArenaCharacterStats.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTER_API UArenaCharacterStats : public UActorComponent
{
	GENERATED_BODY()

public:	

	UArenaCharacterStats();

	void UpdateStat(const FString& StatName, float StatValue);

protected:

	UPROPERTY(EditAnywhere, Category = "Stats", meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
	float HP = 0.5f;
	UPROPERTY(EditAnywhere, Category = "Stats", meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
	float Spd = 0.5f;
};
