#include "ArenaEnemyCharacter.h"
#include "ArenaBaseWeapon.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "ArenaGameMode.h"

AArenaEnemyCharacter::AArenaEnemyCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AArenaEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	SpawnWeapon();
	SpawnDefaultController();
	MGameMode = GetWorld()->GetAuthGameMode<AArenaGameMode>();
}

void AArenaEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AArenaEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AArenaEnemyCharacter::NotifyHit(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	if (Dead) return;
	if (OtherComp->ComponentHasTag("PlayerBullet"))
	{
		Dead = true;
	
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		GetMesh()->SetAllBodiesSimulatePhysics(true);
		GetMesh()->SetCollisionProfileName("Ragdoll");

		GetCharacterMovement()->DisableMovement();

		GetWorld()->GetTimerManager().SetTimer(DeathTimerHandle, this, &AArenaEnemyCharacter::Death, 2.f, false);

		EnemyWeapon->StopFire();
	}
}

void AArenaEnemyCharacter::SpawnWeapon()
{
	const FVector Location(0.0f, 0.0f, 0.0f);
	const FRotator Rotation(0.0f, 0.0f, 0.0f);

	if (Weapon)
	{
		EnemyWeapon = Cast<AArenaBaseWeapon>(GetWorld()->SpawnActor<AActor>(Weapon, Location, Rotation));
		EnemyWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, "GunSocket");
	}
}

void AArenaEnemyCharacter::PlayerInRange()
{
	if (Dead) return;
	EnemyWeapon->StartFire();
}

void AArenaEnemyCharacter::PlayerOutOfRange()
{
	EnemyWeapon->StopFire();
}

void AArenaEnemyCharacter::Death()
{
	MGameMode->DecreseEnemyCounter();
	EnemyWeapon->Destroy();
	Destroy();
}

