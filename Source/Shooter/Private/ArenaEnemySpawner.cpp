#include "ArenaEnemySpawner.h"
#include "ArenaEnemyCharacter.h"
#include "ArenaGameMode.h"

AArenaEnemySpawner::AArenaEnemySpawner()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AArenaEnemySpawner::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(SpawnTimerHandle, this, &AArenaEnemySpawner::Spawn, SpawnRate, true);
	GameMode = GetWorld()->GetAuthGameMode<AArenaGameMode>();
}

void AArenaEnemySpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AArenaEnemySpawner::Spawn()
{
	if (!GameMode->CanEnemiesStillSpwan()) return;

	FVector Location = GetTransform().GetLocation();
	const FQuat Rotation = GetTransform().GetRotation();

	Location.Z += 100.f;

	GetWorld()->SpawnActor<AActor>(PawnToSpawn, Location, Rotation.Rotator());
	
	GameMode->IncreseEnemyCounter();
}

