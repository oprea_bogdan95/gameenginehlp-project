#include "ArenaGameMode.h"

void AArenaGameMode::IncreseEnemyCounter() {
	if (!CanEnemiesStillSpwan()) return;
	EnemyCounter++;
}

void AArenaGameMode::DecreseEnemyCounter()
{
	if (EnemyCounter - 1 < 0) return;
	EnemyCounter--;
}

bool AArenaGameMode::CanEnemiesStillSpwan()
{
	return EnemyCounter + 1 <= MaxSpawnCount;
}
