#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ArenaEnemyCharacter.generated.h"

class AArenaBaseWeapon;
class AArenaGameMode;

UCLASS()
class SHOOTER_API AArenaEnemyCharacter : public ACharacter
{
	GENERATED_BODY()

public:

	AArenaEnemyCharacter();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void NotifyHit( class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;
	
	UFUNCTION()
	void SpawnWeapon();
	UFUNCTION()
	void PlayerInRange();
	UFUNCTION()
	void PlayerOutOfRange();
	UFUNCTION()
	void Death();

	UPROPERTY(EditAnywhere, Category = "Weapon")
	TSubclassOf<AActor> Weapon;

private:
	
	bool Dead = false;
	FTimerHandle DeathTimerHandle;
	
	UPROPERTY()
	AArenaBaseWeapon* EnemyWeapon;
	UPROPERTY()
	AArenaGameMode* MGameMode;
};
